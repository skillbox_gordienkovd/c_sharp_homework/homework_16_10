﻿namespace homework_16_10;

public class MultiThreadExample
{
    private readonly Random _random = new();

    public void PrintParallel()
    {
        for (var i = 0; i < 10; i++)
        {
            var thread = new Thread(Print)
            {
                Name = i.ToString()
            };
            thread.Start();
        }
    }
    
    private void Print()
    {
        Thread.Sleep(_random.Next(1000, 5000));

        Console.WriteLine($"Текст. ThreadName: {Thread.CurrentThread.Name}");
    }
}